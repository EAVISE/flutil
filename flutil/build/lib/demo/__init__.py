"""A module for demonstrating finished models."""
from .inputter import *
from .outputter import *
from .processor import *
